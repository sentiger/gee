package gee

import "net/http"

type Engine struct {
}

func handlerPOST(w http.ResponseWriter, req *http.Request) {
	switch req.URL.Path {
	case "/index":
		w.Write([]byte("index handler"))
		return
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Not found!"))
	}
}
func (engine *Engine) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	// 可以统一处理日志等信息
	switch req.Method {
	case "POST":
		handlerPOST(w, req)
	case "GET":
	default:
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("不支持该方法" + req.Method))
		return
	}
}

func New() *Engine {
	return &Engine{}
}

func (engine *Engine) Run(addr string) error {
	return http.ListenAndServe(addr, engine)
}
