package main

import (
	"gee"
	"log"
)

func main() {
	r := gee.New()
	err := r.Run(":8099")
	if err != nil {
		log.Fatalln(err)
	}
}
