# v1.0.1

# 不进行封装
- 使用下面两种方式比较多
- 这种问题在于如果想监控下每个接口，是不是要每个方法中都要注入监控，比较麻烦，这种设计就是耦合度太低
```
http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		
})
```

# 统一入口
- 统一实现http.Handler接口
- 好处是可以进行统一处理一些共有功能
```
...

func (engine *Engine) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	// 可以统一处理日志等信息
	switch req.Method {
	case "POST":
		handlerPOST(w, req)
	case "GET":
	default:
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("不支持该方法" + req.Method))
		return
	}
}
```

# 弊端
- 如果所有的业务都在这里面处理，会导致这个方法很大
- 对于输出，每次都要调用response实现具体输出方法，包括统一的状态码设置，在每个路由方法中都要统一再写一遍

# todo
- 路由注册
- 要将用户单次请求进行统一封装成context
- 用户的请求操作都是针对这个context，每个请求对应一个context
- 对context进行一些辅助方法来进行输出