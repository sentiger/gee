package gee

type routerGroup struct {
	prefix   string
	basePath string
	parent   *routerGroup
	engine   *Engine
}

func (group *routerGroup) Group(prefix string) *routerGroup {
	engine := group.engine
	newGroup := &routerGroup{
		parent:   group,
		prefix:   prefix,
		basePath: group.basePath + prefix,
		engine:   engine,
	}
	return newGroup
}

func (group *routerGroup) POST(pattern string, handler Handler) {
	group.engine.POST(group.basePath+pattern, handler)
}

func (group *routerGroup) GET(pattern string, handler Handler) {
	group.engine.GET(group.basePath+pattern, handler)
}
