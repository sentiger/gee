package main

import (
	"gee"
	"log"
	"net/http"
)

func main() {
	r := gee.New()
	r.GET("/hello", func(c *gee.Context) {
		c.String(200, "hello")
	})
	v1 := r.Group("/v1")
	{
		v1.GET("/index", func(c *gee.Context) {
			c.String(http.StatusOK, "快捷输出")
		})
		v1.POST("/user", func(c *gee.Context) {
			c.JSON(http.StatusOK, gee.H{
				"code":    200,
				"message": "save user successful",
			})
		})
	}

	v2 := r.Group("/v2")
	{
		v2.GET("/index", func(c *gee.Context) {
			c.String(http.StatusOK, "v2 快捷输出")
		})
		v2.POST("/user", func(c *gee.Context) {
			c.JSON(http.StatusOK, gee.H{
				"code":    200,
				"message": "v2 save user successful",
			})
		})
	}

	err := r.Run(":8099")
	if err != nil {
		log.Fatalln(err)
	}
}
