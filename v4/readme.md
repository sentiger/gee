# v1.0.4

# 目标

- 本次实现的是路由组
- 路由分组好处就是可以将一些共有的路由进行分组，使用统一的前缀，而且最主要的是可以对组进行控制
- 而不是仅仅单单控制单个路由，例如权限控制，路由设置，不可能每次都要在每个方法中去调用相关的方法吧
- 这样的话，就和之前没有封装context是差不多的效果

```go
package main

import (
	"gee"
	"log"
	"net/http"
)

func main() {
	r := gee.New()
	r.GET("/hello", func(c *gee.Context) {
		c.String(200, "hello")
	})
	v1 := r.Group("/v1")
	{
		v1.GET("/index", func(c *gee.Context) {
			c.String(http.StatusOK, "快捷输出")
		})
		v1.POST("/user", func(c *gee.Context) {
			c.JSON(http.StatusOK, gee.H{
				"code":    200,
				"message": "save user successful",
			})
		})
	}

	err := r.Run(":8099")
	if err != nil {
		log.Fatalln(err)
	}
}


```

# 步骤

- 定义一个routerGroup结构体
- 然后添加Group("prefix")方法
- 然后可以通过routerGroup来添加POST，GET方法，然后在这里就处理下pattern加个前缀就行
- 所以就知道路由组有哪些字段了{prefix:"路由前缀",basePath:"这个是路由前缀完整路径，因为可以嵌套子路由组，就不用遍历了"}
- 所以最后为了engin调用Group方便，直接将routerGroup嵌套到engine中

# todo

- 既然实现了routerGroup，那么可以在这个上面实现中间件
