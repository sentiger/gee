package gee

import (
	"fmt"
	"net/http"
)

type H map[string]interface{}
type Handler func(c *Context)

type Engine struct {
	router *router
}

func New() *Engine {
	return &Engine{
		router: &router{handlers: make(map[string]Handler)},
	}
}

func (engine *Engine) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	key := req.Method + "-" + req.URL.Path
	handler, ok := engine.router.handlers[key]
	fmt.Println("寻找key:", key)
	c := newContext(w, req)
	if ok {
		handler(c)
	} else {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(fmt.Sprintf("NOT FOUND %s - %s", req.Method, req.URL.Path)))
	}
}

func (engine *Engine) GET(pattern string, handler Handler) {
	engine.router.addRoute("GET", pattern, handler)
}

func (engine *Engine) POST(pattern string, handler Handler) {
	engine.router.addRoute("POST", pattern, handler)
}

func (engine *Engine) Run(addr string) error {
	return http.ListenAndServe(addr, engine)
}
