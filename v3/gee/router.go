package gee

import (
	"fmt"
)

type router struct {
	handlers map[string]Handler
}

// addRoute 提供内部使用，来方便构造GET,POST等方法
func (r *router) addRoute(method string, pattern string, handler Handler) {
	key := method + "-" + pattern
	fmt.Println("注册->", key)
	r.handlers[key] = handler
}
