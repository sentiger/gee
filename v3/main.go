package main

import (
	"gee"
	"log"
	"net/http"
)

func main() {
	r := gee.New()
	r.GET("/index", func(c *gee.Context) {
		c.String(http.StatusOK, "快捷输出")
	})
	r.POST("/user", func(c *gee.Context) {
		c.JSON(http.StatusOK, gee.H{
			"code":    200,
			"message": "save user successful",
		})
	})
	err := r.Run(":8099")
	if err != nil {
		log.Fatalln(err)
	}
}
