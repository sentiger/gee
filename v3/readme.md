# v1.0.3

# 目标

- 上个版本中遗留下的问题就是对应用户的请求都是没进行封装，都是使用原生的response，request
-

```go
package main

import (
	"gee"
	"log"
	"net/http"
)

func main() {
	r := gee.New()
	r.GET("/index", func(c *gee.Context) {
		c.String(http.StatusOK, "快捷输出")
	})
	r.POST("/user", func(c *gee.Context) {
		c.JSON(http.StatusOK, gee.H{
			"code":    200,
			"message": "save user successful",
		})
	})
	err := r.Run(":8099")
	if err != nil {
		log.Fatalln(err)
	}
}


```

# 步骤

- 抽象context
- 每个http请求都生成一个context，里面就是本次会话的相关内容
- 并在这个会话中提供相关的输出方法

# todo

- router目前只支持map精确查找，但是这个效率是比较高的
- 所以希望框架支持/v1/user/:name这种匹配并且将匹配的结果存放
- 支持模糊匹配/v1/user/*path