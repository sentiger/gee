package gee

import (
	"fmt"
	"net/http"
)

type Context struct {
	Writer  http.ResponseWriter
	Request *http.Request

	handlers []HandlerFunc // 要运行的路由
	index    int
}

func NewContext(w http.ResponseWriter, req *http.Request) *Context {
	return &Context{
		Writer:  w,
		Request: req,
		index:   -1,
	}
}

func (c *Context) Next() {
	c.index++
	s := len(c.handlers)
	for ; c.index < s; c.index++ {
		c.handlers[c.index](c)
		c.Next()
	}
}

// gin 中的处理
//func (c *Context) Next() {
//	c.index++
//	for c.index < len(c.handlers) {
//		c.handlers[c.index](c)
//		c.index++
//	}
//}

func (c *Context) Abort() {
	c.index = len(c.handlers) + 1
}

func (c *Context) Status(statusCode int) {
	c.Writer.WriteHeader(statusCode)
}

func (c *Context) SetHeader(key, value string) {
	c.Writer.Header().Set(key, value)
}

func (c *Context) String(statusCode int, format string, val ...interface{}) {
	c.SetHeader("Context-Type", "text/plain")
	c.Status(statusCode)
	c.Writer.Write([]byte(fmt.Sprintf(format, val...)))
}
