package gee

type routerGroup struct {
	prefix      string
	basePath    string
	parent      *routerGroup
	engine      *Engine
	middlewares []HandlerFunc
}

func (rg *routerGroup) Group(prefix string) *routerGroup {
	newGroup := &routerGroup{
		prefix:   prefix,
		basePath: rg.basePath + prefix,
		parent:   rg,
		engine:   rg.engine,
	}
	rg.engine.groups = append(rg.engine.groups, newGroup)
	return newGroup
}

func (rg *routerGroup) Use(handler ...HandlerFunc) {
	rg.middlewares = append(rg.middlewares, handler...)
}

func (rg *routerGroup) POST(pattern string, handler HandlerFunc) {
	rg.engine.router.addRoute("POST", rg.basePath+pattern, handler)
}

func (rg *routerGroup) GET(pattern string, handler HandlerFunc) {
	rg.engine.router.addRoute("GET", rg.basePath+pattern, handler)
}
