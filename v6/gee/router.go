package gee

import "fmt"

type router struct {
	handlers map[string]HandlerFunc
}

func (r *router) addRoute(method string, pattern string, handler HandlerFunc) {
	key := method + "-" + pattern
	fmt.Println("注册路由====", key)
	r.handlers[key] = handler
}
