package gee

import (
	"fmt"
	"net/http"
	"strings"
)

type HandlerFunc func(c *Context)

type Engine struct {
	*router
	*routerGroup
	groups []*routerGroup
}

func New() *Engine {
	engine := &Engine{
		router: &router{handlers: make(map[string]HandlerFunc, 0)},
		routerGroup: &routerGroup{
			prefix:   "",
			basePath: "",
		},
	}
	engine.routerGroup.engine = engine
	engine.groups = append(engine.groups, engine.routerGroup)

	return engine
}

func Default() *Engine {
	engine := New()
	engine.Use(Recovery())
	return engine
}
func (engine *Engine) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	key := req.Method + "-" + req.URL.Path
	fmt.Println("请求路由====", key)
	handler, ok := engine.router.handlers[key]
	context := NewContext(w, req)

	// todo 可进行缓存优化，无需每次去查找groups
	for _, rg := range engine.groups {
		if strings.HasPrefix(req.URL.Path, rg.basePath) {
			context.handlers = append(context.handlers, rg.middlewares...)
		}
	}
	if ok {
		context.handlers = append(context.handlers, handler)
	} else {
		context.handlers = append(context.handlers, func(c *Context) {
			context.String(http.StatusNotFound, "not found")
			c.Next()
		})
	}
	context.Next()
}

func (engine *Engine) Run(addr string) error {
	return http.ListenAndServe(addr, engine)
}
