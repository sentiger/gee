package main

import (
	"fmt"
	"gee"
)

func main() {
	r := gee.Default()

	r.Use(func(c *gee.Context) {
		//c.String(200, "直接输出")
		//c.Abort()
		fmt.Println("中间件开始")
		c.Next()
		fmt.Print("中间件-end3")
	})

	r.GET("/", func(c *gee.Context) {
		c.String(200, "你好")
	})
	r.POST("/user", func(c *gee.Context) {
		fmt.Println("user 方法")
		c.String(200, "写入成功")
	})

	v1 := r.Group("/v1")
	v1.Use(func(c *gee.Context) {
		fmt.Println("v1中间件-start")
		c.Next()
		fmt.Println("v1中间件-end")
	})
	{
		v1.GET("/hello", func(c *gee.Context) {
			fmt.Println("hello 方法")
			panic("主动抛出异常")
			c.String(200, "hello")
		})
	}

	r.Run(":8080")
}
