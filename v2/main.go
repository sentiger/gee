package main

import (
	"encoding/json"
	"gee"
	"log"
	"net/http"
)

func main() {
	r := gee.New()
	r.GET("/index", func(w http.ResponseWriter, req *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("index page"))
	})
	r.POST("/user", func(w http.ResponseWriter, req *http.Request) {
		w.WriteHeader(http.StatusOK)
		res, _ := json.Marshal(gee.H{
			"code":    200,
			"message": "save success",
		})
		w.Write(res)
	})
	err := r.Run(":8099")
	if err != nil {
		log.Fatalln(err)
	}
}
