# v1.0.2

# 目标

- 在上个版本中有看到弊端，就是业务逻辑导致ServeHTTP这个方法很大，根本维护不了
- 所以这个版本我们就是实现以下这种效果，可以注册路由

```
...

r := gee.New()
r.GET("/index", func(w http.ResponseWriter, req *http.Request) {
    
})
...

```

# 步骤

- 可以定义一个router变量是个map[string]HandlerFunc类型
- 调用r.GET()就是注册[GET-path]handler中
- 在ServeHTTP中就是可以直接找到router中的方法，去调用就行

## router.go

- 添加路由方法addRoute(method,pattern,handler)
- 为了调用方便，在Engine中提供GET,POST等方法给暴露给用户

## gee.go

- 这个里面添加了POST,GET等注册方法

# 弊端

- 虽然这里封装了简单路由，可以通过map来精确定位，但是输出有问题
- 没有对request和response进行封装，每次调用如设置状态，等信息，都是要在每个方法中独立设置
- 没有提供便利的输出方法，获取参数等方法，所以要进行封装

# todo

- 将request，response进行封装成context
- 提供一些常用方法出来，给到用户使用