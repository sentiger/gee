package gee

import (
	"fmt"
	"net/http"
	"strings"
)

type H map[string]interface{}
type Handler func(c *Context)

type Engine struct {
	*routerGroup
	router *router
	groups []*routerGroup
}

func Default() *Engine {
	engine := New()
	engine.Use(Recovery())
	return engine
}

func New() *Engine {
	engine := &Engine{
		router: &router{handlers: make(map[string]Handler)},
	}
	engine.routerGroup = &routerGroup{
		engine: engine,
	}
	engine.groups = append(engine.groups, engine.routerGroup)
	return engine
}

func (engine *Engine) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	key := req.Method + "-" + req.URL.Path
	middlewares := make([]Handler, 0)

	for _, group := range engine.groups {
		if strings.HasPrefix(req.URL.Path, group.basePath) {
			middlewares = append(middlewares, group.middlewares...)
		}
	}

	handler, ok := engine.router.handlers[key]
	fmt.Println("寻找key:", key)
	c := newContext(w, req)
	if ok {
		//handler(c)
		middlewares = append(middlewares, handler)
	} else {
		middlewares = append(middlewares, func(c *Context) {
			c.Status(http.StatusNotFound)
			w.Write([]byte(fmt.Sprintf("NOT FOUND %s - %s", req.Method, req.URL.Path)))
		})
	}
	c.handlers = middlewares
	c.Next()
}

func (engine *Engine) GET(pattern string, handler Handler) {
	engine.router.addRoute("GET", pattern, handler)
}

func (engine *Engine) POST(pattern string, handler Handler) {
	engine.router.addRoute("POST", pattern, handler)
}

func (engine *Engine) Run(addr string) error {
	return http.ListenAndServe(addr, engine)
}
