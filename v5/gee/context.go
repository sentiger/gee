package gee

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// Context http每次请求，会生成一个context，里面是针对本次的所有数据
type Context struct {
	Writer http.ResponseWriter
	Req    *http.Request
	// response info
	StatusCode int
	// middlewares
	handlers []Handler
	index    int
}

func newContext(w http.ResponseWriter, req *http.Request) *Context {
	return &Context{
		Writer: w,
		Req:    req,
		index:  -1,
	}
}

func (c *Context) Next() {
	c.index++
	s := len(c.handlers)
	for ; c.index < s; c.index++ {
		c.handlers[c.index](c)
	}
}

func (c *Context) Status(code int) {
	c.Writer.WriteHeader(code)
}

func (c *Context) SetHeader(key string, value string) {
	c.Writer.Header().Set(key, value)
}

func (c *Context) String(statusCode int, format string, a ...interface{}) {
	c.SetHeader("Content-Type", "text/plain; charset=utf-8")
	c.Status(statusCode)
	c.Writer.Write([]byte(fmt.Sprintf(format, a...)))
}

func (c *Context) JSON(statusCode int, obj interface{}) {
	// 注意：设置头应该在设置状态码之前，否则是会失败。因为http在设置输出码的时候，就发送了头flush
	c.SetHeader("Content-Type", "application/json")
	c.Status(statusCode)
	encode := json.NewEncoder(c.Writer)
	if err := encode.Encode(obj); err != nil {
		http.Error(c.Writer, err.Error(), http.StatusInternalServerError)
	}
}
