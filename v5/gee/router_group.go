package gee

type routerGroup struct {
	prefix      string
	basePath    string
	parent      *routerGroup
	engine      *Engine
	middlewares []Handler
}

func (group *routerGroup) Group(prefix string) *routerGroup {
	engine := group.engine
	newGroup := &routerGroup{
		parent:      group,
		prefix:      prefix,
		basePath:    group.basePath + prefix,
		engine:      engine,
		middlewares: make([]Handler, 0),
	}
	engine.groups = append(engine.groups, newGroup)
	return newGroup
}

func (group *routerGroup) Use(middlewares ...Handler) {
	group.middlewares = append(group.middlewares, middlewares...)
}

func (group *routerGroup) POST(pattern string, handler Handler) {
	group.engine.POST(group.basePath+pattern, handler)
}

func (group *routerGroup) GET(pattern string, handler Handler) {
	group.engine.GET(group.basePath+pattern, handler)
}
