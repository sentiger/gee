# v1.0.4

# 目标

- 本次实现的是路由组添加中间件
- 中间件能无侵入式的添加进框架，并且在进入正式的业务代码里能进行先关修改和响应的修改
- 在路由组的基础上加上中间件比较方便
- 新增了engine.Default()方法，里面注册了默认了Recover中间件

```go
package main

import (
	"fmt"
	"gee"
	"log"
	"net/http"
)

func main() {
	r := gee.New()
	r.Use(func(c *gee.Context) {
		fmt.Println("注册了默认中间件")
		c.Next()
		fmt.Println("注册了默认中间件-end")
	})
	r.GET("/hello", func(c *gee.Context) {
		fmt.Println("hello func")
		c.String(200, "hello")
	})
	v1 := r.Group("/v1")
	v1.Use(func(c *gee.Context) {
		fmt.Println("注册了v1中间件")
		c.Next()
		fmt.Println("注册了v1中间件-end")
	})
	{
		v1.GET("/index", func(c *gee.Context) {
			fmt.Println("v1 index ")
			c.String(http.StatusOK, "快捷输出")
		})
		v1.POST("/user", func(c *gee.Context) {
			c.JSON(http.StatusOK, gee.H{
				"code":    200,
				"message": "save user successful",
			})
		})
	}

	v2 := r.Group("/v2")
	{
		v2.GET("/index", func(c *gee.Context) {
			c.String(http.StatusOK, "v2 快捷输出")
		})
		v2.POST("/user", func(c *gee.Context) {
			c.JSON(http.StatusOK, gee.H{
				"code":    200,
				"message": "v2 save user successful",
			})
		})
	}

	err := r.Run(":8099")
	if err != nil {
		log.Fatalln(err)
	}
}



```

# 步骤

- 因为暴露用户g.Use()，而且中间件应该作用在路由组上的，所以routerGroup提供Use()方法
- routerGroup结构体中新增middlewares切片，用来记录绑定哪些中间件了
- 在engine.ServeHTTP中判断当前路径和所有的路由组basePath是否有关联，是否是basePath开头的，如果是，则要将这个组的middlewares保存下来要运行
- 所以在engine中需要增加一个groups字段保存所有注册的routeGroup
- 现在所有的Handler都记录下来了，依次运行就行，所有运行都在context中运行，所以context需要增加属性handlers,以及运行handler的索引

# todo

- 在中间件中直接输出，后面的中间件直接不运行abort()这种类似gin
