package main

import (
	"fmt"
	"gee"
	"log"
	"net/http"
)

func main() {
	r := gee.Default()
	r.Use(func(c *gee.Context) {
		fmt.Println("注册了默认中间件")
		c.Next()
		fmt.Println("注册了默认中间件-end")
	})
	r.GET("/hello", func(c *gee.Context) {
		fmt.Println("hello func")
		panic("hello")
		c.String(200, "hello")
	})
	v1 := r.Group("/v1")
	v1.Use(func(c *gee.Context) {
		fmt.Println("注册了v1中间件")
		c.Next()
		fmt.Println("注册了v1中间件-end")
	})
	{
		v1.GET("/index", func(c *gee.Context) {
			fmt.Println("v1 index ")
			c.String(http.StatusOK, "快捷输出")
		})
		v1.POST("/user", func(c *gee.Context) {
			c.JSON(http.StatusOK, gee.H{
				"code":    200,
				"message": "save user successful",
			})
		})
	}

	v2 := r.Group("/v2")
	{
		v2.GET("/index", func(c *gee.Context) {
			c.String(http.StatusOK, "v2 快捷输出")
		})
		v2.POST("/user", func(c *gee.Context) {
			c.JSON(http.StatusOK, gee.H{
				"code":    200,
				"message": "v2 save user successful",
			})
		})
	}

	err := r.Run(":8099")
	if err != nil {
		log.Fatalln(err)
	}
}
